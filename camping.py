#!/usr/bin/env python3

import argparse
import json
import logging
from pathlib import Path
import sys
import yaml
from datetime import datetime, timedelta
from dateutil.relativedelta import *

import requests
import requests_random_user_agent
from dateutil import rrule
# from fake_useragent import UserAgent
from dateutil import parser

LOG = logging.getLogger(__name__)
formatter = logging.Formatter("%(asctime)s - %(process)s - %(levelname)s - %(message)s")
sh = logging.StreamHandler()
sh.setFormatter(formatter)
LOG.addHandler(sh)

IGNORED_CAMPSITE_TYPES = [
    'GROUP PICNIC AREA',
    'PICNIC',
]


BASE_URL = "https://www.recreation.gov"
AVAILABILITY_ENDPOINT = "/api/camps/availability/campground/"
MAIN_PAGE_ENDPOINT = "/api/camps/campgrounds/"

INPUT_DATE_FORMAT = "%Y-%m-%d"

SUCCESS_EMOJI = "🏕"
FAILURE_EMOJI = "❌"


def send_slack(content):
    data = {"text": content}
    requests.post("https://hooks.slack.com/services/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", json=data)

def format_date(date_object):
    date_formatted = datetime.strftime(date_object, "%Y-%m-%dT00:00:00.000Z")
    return date_formatted


def generate_params(start, end):
    params = {"start_date": format_date(start), "end_date": format_date(end)}
    return params


def send_request(url, params):
    resp = requests.get(url, params=params)
    if resp.status_code != 200:
        raise RuntimeError(
            "failedRequest",
            "ERROR, {} code received from {}: {}".format(
                resp.status_code, url, resp.text
            ),
        )
    return resp.json()

def get_park_information2(park, park_id, start_date, end_date):
    """
    This function consumes the user intent, collects the necessary information
    from the recreation.gov API, and then presents it in a nice format for the
    rest of the program to work with. If the API changes in the future, this is
    the only function you should need to change.

    The only API to get availability information is the `month?` query param
    on the availability endpoint. You must query with the first of the month.
    This means if `start_date` and `end_date` cross a month bounday, we must
    hit the endpoint multiple times.

    The output of this function looks like this:

    {"<campsite_id>": [<date>, <date>]}

    Where the values are a list of ISO 8601 date strings representing dates
    where the campsite is available.

    Notably, the output doesn't tell you which sites are available. The rest of
    the script doesn't need to know this to determine whether sites are available.
    """

    # Get each first of the month for months in the range we care about.
    start_of_month = datetime(start_date.year, start_date.month, 1)
    months = list(rrule.rrule(rrule.MONTHLY, dtstart=start_of_month, until=end_date))

    # Get data for each month.
    api_data = []
    for month_date in months:
        params = {"start_date": format_date(month_date)}
        LOG.debug("Querying for {} with these params: {}".format(park_id, params))
        url = "{}{}{}/month?".format(BASE_URL, AVAILABILITY_ENDPOINT, park_id)
        resp = send_request(url, params)
        api_data.append(resp)

    # Collapse the data into the described output format.
    # Filter by campsite_type if necessary.
    data = {}
    for month_data in api_data:
        for campsite_id, campsite_data in month_data["campsites"].items():
            available = []
            for date, availability_value in campsite_data["availabilities"].items():
                if availability_value != "Available":
                    LOG.debug("Ignoring campsite because it is not available")
                    continue
                campsite_type = campsite_data["campsite_type"]
                if campsite_type in IGNORED_CAMPSITE_TYPES:
                    LOG.debug(f"Ignoring campsite because `{campsite_type}` is in {IGNORED_CAMPSITE_TYPES}")
                    continue
                dt = datetime.strptime(date, "%Y-%m-%dT00:00:00Z")
                weekday = dt.weekday()
                if weekday in park["weekdays"]:
                    available.append(date)

            if available:
                a = data.setdefault(campsite_id, [])
                a += available

    return data

def get_park_information(park_id, params):
    url = "{}{}{}".format(BASE_URL, AVAILABILITY_ENDPOINT, park_id)
    return send_request(url, params)


def get_name_of_site(park_id):
    url = "{}{}{}".format(BASE_URL, MAIN_PAGE_ENDPOINT, park_id)
    resp = send_request(url, {})
    return resp["campground"]["facility_name"]


def get_num_available_sites(resp, start_date, end_date):
    maximum = resp["count"]

    num_available = []
    num_days = (end_date - start_date).days
    dates = [end_date - timedelta(days=i) for i in range(1, num_days + 1)]
    dates = set(format_date(i) for i in dates)
    print(resp)
    for site in resp["campsites"].values():
        for date, status in site["availabilities"].items():
            if date not in dates:
                continue
            if site["campsite_type"] in IGNORED_CAMPSITE_TYPES:
                continue
            dt = datetime.strptime(date, "%Y-%m-%dT00:00:00Z")
            if status == "Available":
                weekday = dt.weekday()
                LOG.debug("Available site {}: {} on day: {}".format(num_available, json.dumps(site, indent=1), dt.strftime("%A")))
                if weekday == 4 or weekday == 5:
                    print(site["campsite_type"])
                    num_available.append(date)
    return num_available, maximum


def valid_date(s):
    try:
        return datetime.strptime(s, INPUT_DATE_FORMAT)
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def _main(parks):
    out = []
    availabilities = False
    start_date = datetime.now()
    look_ahead_months = args.look_ahead_months
    end_date = datetime.now() + relativedelta(months=+look_ahead_months)
    for park in parks:
        park_id = park["park_id"]
        params = generate_params(start_date, end_date)
        LOG.info(f"Looking for reservations between {start_date} and {end_date}")
        current = get_park_information2(park, park_id, start_date, end_date)

        name_of_site = get_name_of_site(park_id)
        if current:
            emoji = SUCCESS_EMOJI
            availabilities = True
        else:
            emoji = FAILURE_EMOJI

        out.append(
            "{} {} available dates:".format(
                emoji, name_of_site
            )
        )
        for k, dates in current.items():
            for d in dates:
                d_pretty = parser.isoparse(d).strftime(INPUT_DATE_FORMAT)
                out.append(f"{d_pretty[0:10]}")

    output = ", ".join(out)
    if availabilities:
        print(
            "There are campsites available from {} to {}!!!".format(
                start_date.strftime(INPUT_DATE_FORMAT),
                end_date.strftime(INPUT_DATE_FORMAT),
            )
        )
        send_slack(output)
    else:
        print("There are no campsites available :(")
    print(output)


if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--debug", "-d", action="store_true", help="Debug log level")
    argparser.add_argument(
        dest="parks", metavar="park", nargs="+", help="Park ID(s)", type=int
    )
    argparser.add_argument(
        "--look-ahead-months",
        type=int,
        help="Number of months to look ahead for availability",
        default=6,
    )
    argparser.add_argument(
        "--stdin",
        "-",
        action="store_true",
        help="Read list of park ID(s) from stdin instead",
    )

    args = argparser.parse_args()

    if args.debug:
        LOG.setLevel(logging.DEBUG)

    else:
        LOG.setLevel(logging.INFO)

    p = Path( __file__ ).parent.absolute()
    with open(f"{p}/config.yaml", 'r') as config:
        try:
            parks = yaml.load(config)
            _main(parks["parks"])
        except Exception:
            print("Something went wrong")
            send_slack("Reservation.gov cronjob failed")
            raise
